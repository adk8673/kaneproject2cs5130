// Graph.cpp
// CS5130 Project 2
// Implementations of functions from Graph.h
#include<fstream>
#include<list>
#include<sstream>
#include"Graph.h"

using namespace std;

Graph::Graph(string fileLocation)
{
	_fileLocation = fileLocation;
}

// Read in graph from file and describer whether or not it describes a correctly
// described, undirected graph
ValidationErrorListing Graph::ReadAndValidateGraph()
{
	ValidationErrorListing errors;
	ifstream inFile(_fileLocation);

	if (!inFile.is_open())
	{
		errors.FileSuccessfullyRead = false;
		return errors;
	}

	string inputLine;

	list<string> linesFromFile;
	
	// read and store each line
	while (!inFile.eof())
	{
		getline(inFile, inputLine);
		linesFromFile.push_back(inputLine);
	}

	_squareSide = linesFromFile.size();
	_graphBits = new int*[_squareSide];

	// parse every line from the file
	int i = 0;
	for (list<string>::iterator iter = linesFromFile.begin(); iter != linesFromFile.end(); ++iter, ++i)
	{
		_graphBits[i] = new int[_squareSide];

		// we need to make sure that the file is a valid square, each row should have the same number of inputs
		int rowValueCount = 0;
		istringstream stringReader(*iter);
		while (!stringReader.eof())
		{
			int cellValue;
			stringReader >> cellValue;

			// Check that this is a valid bit matrix
			if (!(cellValue == 1
				|| cellValue == 0))
			{
				errors.AreAllCellValuesValid = false;
			}

			if (rowValueCount < _squareSide)
			{
				// Check for self loops along the diagnol
				if (rowValueCount == i
					&& cellValue == 1)
				{
					errors.HasNoLoops = false;
				}

				_graphBits[i][rowValueCount] = cellValue;
			}
			else
			{
				// if we have a value which is more than the number of rows, this isn't a valid matrix
				errors.IsSquare = false;
			}

			rowValueCount++;
		}
	}

	// Last validation criteria - need to verify symmetry
	for (int i = 0; i < _squareSide; ++i)
	{
		for (int j = 0; j < _squareSide; ++j)
		{
			if (_graphBits[i][j] != _graphBits[j][i])
			{
				errors.IsValidUndirected = false;
			}
		}
	}

	return errors;
}

TreeErrorListing Graph::CheckIsTree()
{
	// keep track of which vertices we visited
	bool* vertexVisited = new bool[_squareSide];

	for (int i = 0; i < _squareSide; ++i)
	{
		vertexVisited[i] = false;
	}

	// store type of error from performing breadth first search
	TreeErrorListing treeErrors;
	VisitVertex(vertexVisited, 0, -1, treeErrors);

	// the tree is connected only if the breadth first search visited every single vertex is visited
	bool visitedAllVertices = true;
	for (int i = 0; i < _squareSide; ++i)
	{
		if (!vertexVisited[i])
		{
			visitedAllVertices = false;
		}
	}

	treeErrors.IsConnected = visitedAllVertices;

	return treeErrors;
}

// Recursively step through tree in breadth first search
void Graph::VisitVertex(bool* vertexVisited, int vertexIndex, int visitedFrom, TreeErrorListing& treeErrors)
{
	// Have we previously visited this vertex? If not then 
	// we need to visit all the vertices
	if (!vertexVisited[vertexIndex])
	{
		vertexVisited[vertexIndex] = true;

		for (int i = 0; i < _squareSide; ++i)
		{
			if (_graphBits[vertexIndex][i] == 1 
				&& i != vertexIndex
				&& i != visitedFrom)
			{
				VisitVertex(vertexVisited, i, vertexIndex, treeErrors);
			}
		}
	}
	else
	{
		treeErrors.IsAcyclic = false;
	}
}

// Get the longest path from the tree
list<int>* Graph::GetDiameter()
{
	list<int>* longestPath = NULL;

	// starting with every vertex, check to see what maximum length paths are starting from each vertex
	for (int i = 0; i < _squareSide; ++i)
	{
		list<int>* currentList = GetMaximumPathLength(NULL, i);

		if (longestPath == NULL)
		{
			longestPath = currentList;
		}
		else if (currentList->size() > longestPath->size())
		{
			delete longestPath;
			longestPath = currentList;
		}
		else
		{
			delete currentList;
		}
	}

	return longestPath;
}

// Recusive function to get the maximum length from any single given vertex and return it,
// works only if the graph is really a tree (already checked), else we could get stuck in 
// a infinite amount of recursions
list<int>* Graph::GetMaximumPathLength(list<int>* currentPath, int currentVertex)
{
	if (currentPath == NULL)
	{
		currentPath = new list<int>();
	}

	currentPath->push_back(currentVertex);
	list<int>* maxPath = new list<int>;
	*maxPath = *currentPath;
	
	for (int i = 0; i < _squareSide; ++i)
	{
		if (!ListContainsVertice(currentPath, i)
			&& _graphBits[currentVertex][i] == 1)
		{
			list<int>* newPath = new list<int>;
			*newPath = *currentPath;
			
			newPath = GetMaximumPathLength(newPath, i);

			if (maxPath == NULL)
			{
				maxPath = newPath;
			}
			else if (newPath->size() > maxPath->size())
			{
				delete maxPath;
				maxPath = newPath;
			}
			else
			{
				delete newPath;
			}
		}
	}

	delete currentPath;
	return maxPath;
}

// Helper function to see if a vertex is already in this path
bool Graph::ListContainsVertice(list<int>* currentPath, int vertex)
{
	bool containsValue = false;

	for (std::list<int>::iterator iter = currentPath->begin(); iter != currentPath->end(); ++iter)
	{
		if (*iter == vertex)
		{
			containsValue = true;
		}
	}

	return containsValue;
}

// Graph.h
// CS5130 Project 2
// class representing a graph data object
#ifndef GRAPH_H
#define GRAPH_H

#include<iostream>
#include<list>
#include<string>

// struct to describe validation errors during parsing
struct ValidationErrorListing {
	bool IsSquare = true;
	bool HasNoLoops = true;
	bool IsValidUndirected = true;
	bool AreAllCellValuesValid = true;
	bool FileSuccessfullyRead = true;
};

// struct to describe whether or not a graph is a tree or why
struct TreeErrorListing {
	bool IsConnected = true;
	bool IsAcyclic = true;
};

class Graph {
public:
	Graph(std::string);
	ValidationErrorListing ReadAndValidateGraph();
	TreeErrorListing CheckIsTree();
	std::list<int>* GetDiameter();

	// Used only for debugging, just writes out contents of the read in matrix
	void WriteOutGraphMatrix()
	{
		for (int i = 0; i < _squareSide; ++i)
		{
			for (int j = 0; j < _squareSide; ++j)
			{
				std::cout << _graphBits[i][j] << "\t";
			}

			std::cout << "\n";
		}
	}

private:
	void VisitVertex(bool *, int, int, TreeErrorListing&);
	std::list<int>* GetMaximumPathLength(std::list<int> *, int);
	bool ListContainsVertice(std::list<int> *, int);

	int _squareSide = 0;
	std::string _fileLocation;
	bool _isInitialized = false;
	int** _graphBits = NULL;
};


#endif
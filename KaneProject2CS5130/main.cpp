// main.cpp
// CS5130 Project 2
// Main driver application
#include<iostream>
#include"Graph.h"

using namespace std;

bool CheckValidation(Graph &);
bool CheckIsTree(Graph &);

int main(int argc, char** argv)
{
	if (argc != 2)
	{
		cout << "ERROR: Invalid number of command line arguments, need exactly one (filename of adjaceny file to be read)";
		return 1;
	}

	string fileName = argv[1];
	Graph graph(fileName);
	graph.WriteOutGraphMatrix();
	
	ValidationErrorListing validationErrors = graph.ReadAndValidateGraph();
	bool continueProcessing = CheckValidation(graph);
	
	if (continueProcessing)
	{
		continueProcessing = CheckIsTree(graph);
	}

	if (continueProcessing)
	{
		list<int>* longestPath = graph.GetDiameter();

		cout << "The length of the longest path of the tree is " << longestPath->size() << endl
			<< "The path is: " << endl;

		for (list<int>::iterator iter = longestPath->begin(); iter != longestPath->end(); ++iter)
		{
			// load values in as 0 based but need to add one to make sure the values show up correctly
			cout << *iter + 1 << endl;
		}
	}

	return 0;
}

bool CheckValidation(Graph& graph)
{
	ValidationErrorListing validationErrors = graph.ReadAndValidateGraph();

	bool isTreeValid = true;
	if (!validationErrors.AreAllCellValuesValid)
	{
		cout << "Input file contained values that were not valid (all values must be either 1 or 0)" << endl;
		isTreeValid = false;
	}

	if (!validationErrors.HasNoLoops)
	{
		cout << "Input file had vertices with self-loops" << endl;
		isTreeValid = false;
	}

	if (!validationErrors.IsSquare)
	{
		cout << "Input file contained at least one row that had more or less values than the number of rows" << endl;
		isTreeValid = false;
	}

	if (!validationErrors.IsValidUndirected)
	{
		cout << "Input values didn't contain symmetrical entries in adjacency matrix and therefore can't represent an undirected matrix" << endl;
		isTreeValid = false;
	}

	if (!validationErrors.FileSuccessfullyRead)
	{
		cout << "Failed to open passed filename" << endl;
		isTreeValid = false;
	}

	return isTreeValid;
}

bool CheckIsTree(Graph& graph)
{
	bool isValidTree = true;
	TreeErrorListing treeErrors;
	treeErrors = graph.CheckIsTree();

	if (!treeErrors.IsAcyclic)
	{
		cout << "Input values represent a graph which contains a cycle, and therefore is not a tree" << endl;
		isValidTree = false;
	}

	if (!treeErrors.IsConnected)
	{
		cout << "Input values represent a graph which is not connected, and therefore is not a tree" << endl;
		isValidTree = false;
	}

	return isValidTree;
}